# WeatherApp

![Badge](https://forthebadge.com/images/badges/built-with-love.svg)
![Badge](https://forthebadge.com/images/badges/made-with-python.png)
[![Quality Gate Status](https://sonar.info.univ-lyon1.fr/api/project_badges/measure?project=qdev-s5_qdev-s5-back-end_AY03lQhpu4zgX3p-IQno&metric=alert_status&token=sqb_40313321d4638ca3ba6e44e818f608e09bafc299)](https://sonar.info.univ-lyon1.fr/dashboard?id=qdev-s5_qdev-s5-back-end_AY03lQhpu4zgX3p-IQno)
![Badge](https://forthebadge.com/images/badges/docker-container.png)

Cette application vous permet de consulter la température d'une ville à partir d'une barre de recherche, d'autres informations sont disponibles telles que l'humidité, la vitesse du vent...

## Équipe

### Back End
- **Anas Daoui**: Développeur Django 
- **Ilyas Meskine**: Développeur Django 

## Technologies Utilisées

- Back-end: Python, Django, pycov, pytest, request
- API: OpenWeatherAPI
- Autres outils: CI/CD, tests unitaires, Sonarqube, Docker
## Comment Installer et Déployer

 - Importer le répertoire WeatherApp-front-end avec la commande ```git clone https://forge.univ-lyon1.fr/qdev-s5/qdev-s5-back-end.git```
 - Le placer au même niveau que le répertoire WeatherApp-front
 - Ouvrir l'invite de commande dans le répertoire et lancer la commande : 
 ```docker-compose -f .\docker-compose.yaml up```

### Prérequis

- L'application nécessite une connexion internet pour se connecter à l'API

Linux
- Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre système :

        - Docker : Installer Docker
        - Docker Compose : Installer Docker Compose

Windows
- Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre système :

        - Docker Desktop : Installer Docker Desktop

