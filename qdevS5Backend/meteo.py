import requests
import os
from django.views.decorators.http import require_GET

@require_GET
def get_actual_weather(request,city):
    api_key = os.environ.get('OPENWEATHER_API_KEY')
    url = (f"https://api.openweathermap.org/data/2.5/weather?q={city}\
        &appid={api_key}&units=metric&lang=fr")
    reponse = requests.get(url)
    if reponse.status_code == 200:
        return reponse.json()
    else:
        return None

@require_GET    
def get_prevision_weather(request,city):
    api_key = os.environ.get('OPENWEATHER_API_KEY')
    url = (f"https://api.openweathermap.org/data/2.5/forecast?q={city}\
           &appid={api_key}&units=metric&lang=fr")
    reponse = requests.get(url)
    if reponse.status_code == 200:
        return reponse.json()
    else:
        return None
