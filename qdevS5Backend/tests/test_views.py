from django.test import TestCase, Client

class FetchDataTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_fetch_data(self):
        response = self.client.get('/fetch-data/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(len(response.json()) > 0)



class WeatherURLTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_actual_weather_url(self):
        response = self.client.get('/actual_weather/Lyon/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('name', response.json())

    def test_prevision_weather_url(self):
        response = self.client.get('/prevision_weather/Lyon/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('list', response.json())
        dates = [item['dt_txt'][:10] for item in response.json().get('list', [])]
        self.assertTrue(len(set(dates)) > 1)


