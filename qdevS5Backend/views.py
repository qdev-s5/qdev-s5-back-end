import requests
from django.http import JsonResponse
from .meteo import get_actual_weather, get_prevision_weather
from django.views.decorators.http import require_GET


def fetch_data(request): # NOSONAR
    print("Vue fetch_data exécutée")  # Pour le débogage
    response = requests.get('https://jsonplaceholder.typicode.com/posts')
    print("Réponse de l'API externe :", response.status_code)
    return JsonResponse(response.json(), safe=False)

@require_GET 
def view_actual_weather(request, city):
    weather_data = get_actual_weather(request,city)
    return JsonResponse(weather_data, safe=False)

@require_GET 
def view_prevision_weather(request, city):
    prevision_data = get_prevision_weather(request,city)
    return JsonResponse(prevision_data, safe=False)
